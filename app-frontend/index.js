import $ from 'jquery';
import 'bootstrap';
import './bootstrap.scss';
import './swiper';
import './slider';
import './shuffle';
import './list';
import './hide';
import './gallery';
import './scroll';
import './count';


$(() => {
  $('.no-link')
    .click((e) => {
      e.preventDefault();
    });
});
