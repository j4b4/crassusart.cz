import $ from 'jquery';
import Shuffle from 'shufflejs';
import _ from 'lodash';


function getSortBy(attribute) {
  return element => element.getAttribute(`data-${attribute}`);
}

function startSorting(shuffleInstance, sortSelector, sortOptions) {
  const sort = () => {
    const value = $(sortSelector).val();
    const options = sortOptions[value] || {};
    shuffleInstance.sort(options);
  };

  $(sortSelector).change(sort);

  sort();
}

function filterByFilters(shuffleInstance, filters, filterSelector) {
  $(filterSelector).removeClass('active');
  filters.forEach((filter) => {
    $(`${filterSelector}[data-filter="${filter}"]`).addClass('active');
  });
  shuffleInstance.filter((element) => {
    if (filters.length === 0) {
      return true;
    }
    const groups = JSON.parse(element.getAttribute('data-groups'));
    return _.difference(filters, groups).length === 0;
  });
}

$(() => {
  if ($('#engines-list').length === 1) {
    const shuffleInstance = new Shuffle($('#engines-list'), {
      itemSelector: '.engine-row',
    });

    const sortOptions = {
      price: {
        by: getSortBy('price'),
        reverse: true,
      },
      name: {
        by: getSortBy('name'),
        reverse: false,
      },
    };

    startSorting(shuffleInstance, '.engines-list-sort', sortOptions);

    const filters = [];

    $('.engine-list-filter').click((e) => {
      const filter = $(e.target).data('filter');
      const filterParts = filter.split('-');
      const filterName = filterParts.shift();

      if (filters.includes(filter)) {
        _.remove(filters, item => item === filter);
        filterByFilters(shuffleInstance, filters, '.engine-list-filter');
        return;
      }

      _.remove(filters, item => item.startsWith(`${filterName}-`));
      filters.push(filter);
      filterByFilters(shuffleInstance, filters, '.engine-list-filter');
    });
  }

  if ($('#model-car-list').length === 1) {
    const shuffleInstance = new Shuffle($('#model-car-list'), {
      itemSelector: '.model-car',
    });
    const selectedCats = [];
    const selectedProps = [];

    function filterItems() {
      active();
      shuffleInstance.filter((element) => {
        if (selectedProps.includes('new')) {
          if (element.getAttribute('data-new') !== '1') {
            return false;
          }
        }
        if (selectedProps.includes('stock')) {
          if (element.getAttribute('data-stock') !== '1') {
            return false;
          }
        }
        if (selectedCats.length === 0) {
          return true;
        }
        const cat = element.getAttribute('data-cat');
        return selectedCats.includes(cat);
      });
    }

    function activeButtons() {
      $('.shuffle-filter-button')
        .each(function () {
          const el = $(this);
          const cat = el.data('cat');
          if (selectedCats.includes(cat)) {
            el.addClass('active');
          } else {
            el.removeClass('active');
          }
        });
    }

    function activeCheckboxes() {
      $('.shuffle-filter-checkbox')
        .each(function () {
          const el = $(this);
          const prop = el.data('prop');
          el.prop('checked', selectedProps.includes(prop));
        });
    }

    function active() {
      activeButtons();
      activeCheckboxes();
    }

    $('.shuffle-filter-button')
      .click(function () {
        const cat = $(this)
          .data('cat');
        if (selectedCats.includes(cat)) {
          _.pull(selectedCats, cat);
        } else {
          selectedCats.push(cat);
        }
        filterItems();
      });

    $('.shuffle-filter-checkbox')
      .click(function () {
        const prop = $(this)
          .data('prop');
        if (selectedProps.includes(prop)) {
          _.pull(selectedProps, prop);
        } else {
          selectedProps.push(prop);
        }
        filterItems();
      });

    $('.shuffle-filter-clear')
      .click(() => {
        selectedCats.length = 0;
        filterItems();
      });

    const sortOptions = {
      price: {
        reverse: true,
        by: getSortBy('price'),
      },
      sold: {
        reverse: true,
        by: getSortBy('sold'),
      },
    };

    startSorting(shuffleInstance, '.shuffle-sort-button', sortOptions);
  }
});
