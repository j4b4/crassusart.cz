import $ from 'jquery';

window.$ = $;
$('.scroll-a').click((e) => {
  const href = $(e.currentTarget).attr('href');
  const mainNavHeight = $('#main-nav').height();
  $('html, body').animate({
    scrollTop: ($(href).offset().top) - mainNavHeight,
  }, 800);
});
