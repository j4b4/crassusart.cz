import $ from 'jquery';
import './node_modules/nouislider/distribute/nouislider.css';
import noUiSlider from 'nouislider';

$(() => {
  const sliderEl = $('#priceSlider');
  if (sliderEl.length === 1) {
    const priceMin = sliderEl.data('price-min');
    const priceMax = sliderEl.data('price-max');
    const priceFrom = sliderEl.data('price-from') || priceMin;
    const priceTo = sliderEl.data('price-to') || priceMax;
    const slider = noUiSlider.create(
      document.getElementById('priceSlider'),
      {
        start: [priceFrom, priceTo],
        connect: true,
        step: 1000,
        range: {
          min: priceMin,
          max: priceMax,
        },
      },
    );

    slider.on('update', (e) => {
      const from = Number.parseInt(e[0])
        .toLocaleString('cs-CZ', {
          style: 'decimal',
        });
      const to = Number.parseInt(e[1])
        .toLocaleString('cs-CZ', {
          style: 'decimal',
        });
      $('[name="priceFrom"]')
        .val(from);
      $('[name="priceTo"]')
        .val(to);
    });
  }
});
