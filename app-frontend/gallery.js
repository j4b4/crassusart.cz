import $ from 'jquery';
import 'magnific-popup/dist/jquery.magnific-popup';
import 'magnific-popup/dist/magnific-popup.css';

$(() => {
  $('.images-popup').each(function () {
    const el = $(this);
    const images = [];
    $('.images-popup-images div').each(function () {
      images.push({
        src: $(this).data('img'),
      });
    });
    const index = el.data('index');
    el.magnificPopup({
      items: images,
      index: index || 0,
      gallery: {
        enabled: true,
      },
      type: 'image', // this is default type
    });
  });
});
