import $ from 'jquery';
import List from 'list.js';

function updatePaginationClasses() {
  $('#dealersList .pagination li').addClass('page-item');
  $('#dealersList .pagination li a').addClass('page-link');
}

$(() => {
  if ($('#dealersList').length === 1) {
    const dealersList = new List('dealersList', {
      valueNames: ['title', 'address'],
      pagination: true,
      page: 10,
    });

    updatePaginationClasses();
    $('#dealersList .pagination').bind('DOMSubtreeModified', updatePaginationClasses);
  }
});
