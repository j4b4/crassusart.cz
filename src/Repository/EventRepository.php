<?php
/**
 * Created by PhpStorm.
 * User: jmerta
 * Date: 1.3.19
 * Time: 16:49
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class EventRepository
 * @package App\Repository
 */
class EventRepository extends EntityRepository {

    public function findEventToHP() {
       return $this->getEntityManager()->createQuery(
           'Select e from App:Event e WHERE e.showFrom <= :showFrom AND e.showTo >= :showTo ORDER BY e.id DESC'
       )->setParameter('showFrom', date('Y-m-d G:i:s'))
           ->setParameter('showTo', date('Y-m-d G:i:s'))
           ->setMaxResults(1)->getOneOrNullResult();
    }
}
