<?php
/**
 * Created by PhpStorm.
 * User: jmerta
 * Date: 1.3.19
 * Time: 16:49
 */

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class AdviserNewsRepository
 * @package App\Repository
 */
class AdviserNewsRepository extends EntityRepository {

    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUnreadNews(User $user) {
       $qb =  $this->createQueryBuilder('n');

       $qb->leftJoin('n.readers', 'r');
       $qb->where('r.id != :userId OR r is null');
       $qb->setParameter('userId', $user->getId());
       $qb->orderBy('n.id', 'DESC');
       $qb->setMaxResults(5);

       return $qb->getQuery()->getResult();
    }


}
