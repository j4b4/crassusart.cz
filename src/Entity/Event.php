<?php
/**
 * Created by PhpStorm.
 * User: jmerta
 * Date: 27.2.19
 * Time: 23:39
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Event
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event {

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={    })
     */
    protected $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $headline;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $text;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $showFrom;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $showTo;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getHeadline(): ?string {
        return $this->headline;
    }

    /**
     * @param string|null $headline
     */
    public function setHeadline(?string $headline): void {
        $this->headline = $headline;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void {
        $this->text = $text;
    }

    /**
     * @return \DateTime
     */
    public function getShowFrom() {
        return $this->showFrom;
    }

    /**
     * @param \DateTime $showFrom
     */
    public function setShowFrom(\DateTime $showFrom): void {
        $this->showFrom = $showFrom;
    }

    /**
     * @return \DateTime
     */
    public function getShowTo() {
        return $this->showTo;
    }

    /**
     * @param \DateTime $showTo
     */
    public function setShowTo(\DateTime $showTo): void {
        $this->showTo = $showTo;
    }

}
