<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * Class AdviserNews
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\AdviserNewsRepository")
 */
class AdviserNews {

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={    })
     */
    protected $id;

    /**
     * @var Category|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $text;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(
     *     targetEntity="User"
     * )
     * @ORM\JoinTable(name="news_readers", joinColumns={@JoinColumn(name="news_id", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id"
     * )}
     * )
     */
    protected $readers;

    public function __construct() {
        $this->readers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void {
        $this->text = $text;
    }

    /**
     * @return ArrayCollection
     */
    public function getReaders(): Collection {
        return $this->readers;
    }

    /**
     * @param ArrayCollection $readers
     */
    public function setReaders(ArrayCollection $readers): void {
        $this->readers = $readers;
    }

    /**
     * @param User $user
     */
    public function addReader(User $user): void {
        if (!$this->readers->contains($user)) {
            $this->readers->add($user);
        }
    }

    /**
     * @param User $user
     */
    public function removeReader(User $user): void {
        if ($this->readers->contains($user)) {
            $this->readers->removeElement($user);
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function ireadit(User $user): bool {
        return $this->readers->contains($user);
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category {
        return $this->category;
    }

    /**
     * @param Category|null $category
     */
    public function setCategory(?Category $category): void {
        $this->category = $category;
    }



}
