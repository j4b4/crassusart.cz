<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;

class ComplaintController extends AbstractController
{
    /**
     * @Route("/reklamace")
     * @param Request $requestch
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $defaultData = ['message' => 'Zde nám napište, co se vám nelíbilo...'];
        $form = $this->createFormBuilder($defaultData)
            ->add('name', TextType::class, ['label' => 'Jméno', 'constraints' => [new NotBlank()]])
            ->add('surname', TextType::class, ['label' => 'Příjmení', 'constraints' => [new NotBlank()]])
            ->add('email', EmailType::class, ['label' => 'Email', 'constraints' => [new NotBlank()]])
            ->add('message', TextareaType::class, ['label' => 'Zpráva', 'constraints' => [new NotBlank()]])
            ->add('send', SubmitType::class, [
                'attr' => ['class' => 'btn-danger'],
                'label' => 'Odeslat'
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }


        return $this->render('complaint/index.html.twig', [
            'controller_name' => 'ComplaintController',
            'form' => $form->createView()
        ]);
    }
}
