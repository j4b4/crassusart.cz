<?php

namespace App\Controller;

use App\dataObj\staticData\Sections;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InvestmentController extends AbstractController
{
    /**
     * @Route("/investice")
     */
    public function index()
    {
        return $this->render('investment/index.html.twig', [
            'controller_name' => 'InvestmentController',
            'investmentSections' => Sections::getInvestmentData()
        ]);
    }
}
