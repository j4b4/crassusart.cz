<?php

namespace App\Controller;

use App\dataObj\staticData\Positions;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class KarieraController extends AbstractController
{
    /**
     * @Route("/kariera", name="kariera")
     */
    public function index()
    {
        return $this->render('kariera/index.html.twig', [
            'controller_name' => 'KarieraController',
            'positions' => Positions::getData(),
        ]);
    }
}
