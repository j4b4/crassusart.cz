<?php

namespace App\dataObj\declarations;



class AssistanceDataObj
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $img;

    /**
     * @var string
     */
    public $link;


    /**
     * @var string[]
     */
    public $numbers;

    /**
     * AssistanceDataObj constructor.
     * @param string $img
     * @param string $link
     * @param string[] $numbers
     */
    public function __construct(string $name, string $img, string $link, array $numbers)
    {
        $this->name = $name;
        $this->img = $img;
        $this->link = $link;
        $this->numbers = $numbers;
    }


}
