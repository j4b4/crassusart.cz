<?php

namespace App\dataObj\declarations;



class BossDataObj
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $role;

    /**
     * @var string
     */
    public $img;

    /**
     * @var string
     */
    public $text;

    /**
     * BossDataObj constructor.
     * @param string $name
     * @param string $role
     * @param string $img
     * @param string $text
     */
    public function __construct($name, $role, $img, $text)
    {
        $this->name = $name;
        $this->role = $role;
        $this->img = $img;
        $this->text = $text;
    }


}
