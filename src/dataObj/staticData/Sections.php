<?php

namespace App\dataObj\staticData;


use App\dataObj\declarations\components\SectionDataObj;


class Sections
{
    public static function getInsuranceData()
    {
        return array(

            new SectionDataObj(
                "životní<br>pojištění",
                "SNADNÁ POMOC V NEPŘÍZNIVÝCH ŽIVOTNÍCH SITUACÍ",
                "zivotko",
                "Zivotni.svg",
                "Zivotni-vyber.svg",
                [
                    "Životní pojištění, lidově „úrazovka“, slouží k finančnímu zajištění rodiny v případě nenadálých životních událostí. Hlavním cílem pojištění je ochrana příjmu rodiny.

ŽP je dlouhodobý produkt, který nabízí i možnost zhodnocení finančních prostředků a vytváření finančních rezerv na důchod. Klient má možnost snížit daňový základ až o 24 000 Kč ročně, a každý rok tak ušetřit až 3 600 Kč na dani, a zároveň získat od svého zaměstnavatele příspěvek až 50 000,-Kč.

Podstatným důvodem uzavření životního pojištění je kombinace pojistné ochrany s možností vytváření finanční rezervy. Současná podoba životního pojištění je variabilní a jeho parametry lze měnit tak, aby vyhovovaly aktuální potřebě klienta.

Uzavřít životní pojištění je důležitým životním krokem a pokud chcete, aby Vaše „životka“ měla dobré finanční krytí, je více než vhodné konzultovat její sestavení s kvalifikovaným odborníkem.

Přijďte se tedy poradit s našimi nezávislými finančními poradci, kteří Vám v rozhodování a výběru ochotně pomohou.

Víte, že Vás může správně sjednané ŽP zachránit až od osobního bankrotu či exekuce?"
                ]
            ),
            new SectionDataObj(
                "neživotní<br>pojištění",
                "POMÁHÁME K ŘÁDNÉMU POJIŠTĚNÍ VAŠEHO MAJETKU",
                "nezivotko",
                "Nezivotni.svg",
                "Nezivotni-vyber.svg",
                [
                    "Pro ochranu Vašeho rodinného domu, bytu nebo auta je zřízení kvalitního pojištění velmi důležitou událostí.

Při výběru svého pojištění je potřeba dbát nejen na jeho cenu, ale také na výši pojistné ochrany, na výluky v pojištění a na schopnost pojišťovny řádně provést likvidaci pojistné události v krátkém čase. Na základě toho doporučujeme zvolit vhodnou pojistnou částku (POZOR na podpojištění) a sjednat pojištění pouze u významných pojišťoven v ČR.

Pojištění aut je často podceňovanou záležitostí. Povinné ručení a havarijní pojištění je dnes již velmi variabilním pojištěním, na kterém lze sice hodně peněz ušetřit, ale zároveň se můžete v případě dopravní nehody dostat do velmi nepříjemné finanční tísně.

Doporučujeme svým klientům pravidelně kontrolovat pokles ceny svého vozidla a její aktualizace na pojistné smlouvě. Důvodem je možné snížení základní ceny auta v pojistné smlouvě. "

                ]
            ),
            new SectionDataObj(
                "firemní<br>pojištění",
                "POMÁHÁME RŮSTU SPOLEČNOSTEM A VYTVÁŘENÍ ZISKU",
                "firma",
                "Firemni.svg",
                "Firemni-vyber.svg",
                [
                    "Naše společnost se věnuje poskytování poradenských služeb také v oblasti pojištění malých a středních firem, obcí, SVJ a bytových družstev.

Na základě našeho poradenství získávají společnosti jistotu v oblasti pojištění majetku, aut a odpovědnosti. Na základě individuálních potřeb firmy hledáme optimální umístění pojištění u takových pojišťoven, které jsou schopny řádně plnit své závazky z pojistné smlouvy vůči klientovi.

Naše kvalifikované poradenství umožňuje společnostem optimalizovat své náklady, věnovat se bezpečně svému hlavnímu předmětu podnikání, růstu společnosti a vytváření zisku. Součástí naší nabídky je dlouhodobá a pravidelná správa smluv, případně pojistných událostí. Máme zájem, aby se všechny likvidace uskutečnily řádně a v co nejkratším termínu.

Společnostem jsme schopni navrhnout motivační program pro zaměstnance. Benefity zaměstnancům navrhujeme vždy individuálně s ohledem na strukturu a potřeby společnosti. Zároveň hledáme možnosti optimalizovat mzdové náklady společnosti a maximálně využít daňové úlevy.",

                ]
            )
        );
    }
    public static function getInvestmentData()
    {
        return array(

            new SectionDataObj(
                "Investiční<br>fondy",
                "",
                "investice",
                "fondy.svg",
                "fondy-vyber.svg",
                [
                    "Vytváříme investiční řešení šité na míru dle požadovaných cílů klienta. Ke krátkodobému vytváření
rezervy, spoření pro děti, na koupi domu, na dostatečné rezervy na důchod nebo zhodnocení volného
kapitálu. Investiční produkty nabízejí pravidelné nebo jednorázové investování. Portfolia na míru
sestavujeme nejen pro fyzické osoby, ale také pro firmy, bytová družstva či obce Spolupracujeme s
největšími investičními společnostmi v ČR a na zahraničních trzích. Vždy hledáme pro klienty
nejvýhodnější produkty s ohledem na předpokládanou délku investice, profit, náklady a rizikovost.",
                    "Víte, že díky dobře zvolené finanční strategii, se z Vás může stát finančně nezávislý člověk (rentiér) a
nemusíte se spoléhat na starobní důchod?"
                ]
            ),
            new SectionDataObj(
                "Spoření",
                "",
                "savings",
                "sporeni.svg",
                "sporeni-vyber.svg",
                [
                    "Stavební spoření je velmi oblíbený šestiletý produkt využívající ke zhodnocování
vašich úspor úrokovou sazbu a hlavně státní příspěvek, který může být až
12 000 Kč za celou dobu spoření při ročním spoření klienta 20 000,-Kč/ročně.
Finanční prostředky jsou pojištěny do výše 100%. Další výhodou tohoto
produktu je možnost financovat bydlení pomocí úvěru ze stavebního spoření.",
                    "Termínované vklady nabízíme klientům, kteří chtějí uložit najednou větší obnos
peněz, jež nebudou potřebovat po předem určenou dobu. Největší výhodou
termínovaných vkladů je garantovaná úroková sazba a pojištění vkladu."
                ]
            ),
            new SectionDataObj(
                "podílové<br>fondy",
                "",
                "fonds",
                "fondy.svg",
                "fondy-vyber.svg",
                [
                    "Při investování do nebankovních produktů vybíráme z široké škály investičních
nástrojů. Fyzické osoby po 3 letech investice do fondů neplatí daň z příjmu.",
                    "Akciové fondy jsou vhodné jako dynamická složka vašeho investičního portfolia.
Investovat můžete do různých tržních segmentů či lokalit. Největší americké
firmy, evropský trh farmacie či snad informační technologie asijských draků?
Akciové fondy patří mezi nejrizikovější fondy a jsou vhodné na delší dobu
investování.",
                    "Dluhopisové fondy patří mezi konzervativní fondy. Dluhopisové fondy investují
do státních nebo korporátních dluhopisů. Tyto fondy nabízejí stabilnější, ale
nižší výnos.",
                    "Smíšené fondy kombinují výhody akciových a dluhopisových fondů.",
                    "Realitní fondy. Rádi by jste investovali do nemovitostí, ale nemáte dostatečný
kapitál? Pak je tento typ fondu právě pro Vás. Realitní fondy investují do koupí
nemovitostí s dlouhodobými nájemníky a Vaše investice tak nabývá na hodnotě
nejen díky růstu cen nemovitostí, ale také díky podílům z nájmů. Navíc přesně
víte, do kterých budov jsou Vaše peníze investovány.",
                    "ETF fondy jsou nízkonákladové fondy kopírující vždy určitý burzovní index. Tyto
fondy jsou na evropském trhu stále ještě novinkou, avšak díky výrazně nižším
poplatkům se postupně dostávají do investičních trendů. Výběr je opět veliký,
nejznámějším a nejvyhledávanějším indexem je bezesporu akciový S&amp;P 500
(500 nejsilnějších firem USA),",
                    "Dalšími možnostmi investování s námi jsou také komoditní fondy, peněžní
fondy, dividendové fondy či zlato."
                ]
            )

        );
    }
    public static function getLoansData()
    {
        return array(

            new SectionDataObj(
                "HYPOTÉKY",
                "",
                "hypoteky",
                "hypoteky.svg",
                "hypoteky-vyber.svg",
                [
                    "Spotřebitelské úvěry na bydlení – hypoteční úvěry jsou účelovým úvěrem sloužícím k
financování bytových potřeb – koupě, výstavba či rekonstrukce, k vypořádání práv k nemovité
věci, k profinancování dříve zaplacených vlastních prostředků investovaných do nemovitosti
apod. Hypoteční úvěry jsou zajišťovány zástavním právem k nemovitosti.
Úroky z hypotečních úvěrů si lze odečíst od základu daně. Při výběru nejvhodnějšího
hypotečního úvěru je nutné posoudit různé požadavky bank na způsob čerpání, požadavky na
zajištění úvěru, nutnosti zřízení běžného účtu atd. Více informací Vám rádi poskytneme nad
šálkem kávy."
                ]
            ),
            new SectionDataObj(
                "SPOTŘEBITELSKÉ<br>ÚVĚRY",
                "",
                "loans",
                "spotrebitelsky_uver.svg",
                "spotrebitelsky_uver-vyber.svg",
                [
                    "Spotřebitelské úvěry je jeden z úvěrů určený pro jednotlivce na nepodnikatelské účely,
nejčastěji nákup (spotřebního zboží, automobilu, vybavení domácnosti a podobně), dále také k
refinancování již stávajících dluhů, půjček. Při sjednání úvěrů je nutné vždy posoudit výši RPSN.
Výhodou těchto úvěrů je i rychlost vyřízení a čerpaní finančních prostředků, přijďte se o tom
přesvědčit."
                ]
            ),
            new SectionDataObj(
                "STAVEBNÍ<br>SPOŘENÍ",
                "",
                "stavebni",
                "stavebni_sporeni.svg",
                "stavebni_sporeni-vyber.svg",
                [
                    "Úvěry ze stavebního spoření se využívají pro financování pořízení
nemovitosti i v družstevním vlastnictví, rekonstrukce či koupě stavebního pozemku. Klienti
mohou využít řádný nebo překlenovací úvěr. Úroky z úvěrů si lze odečíst od základu daně. Jaké
jsou podmínky k získání úvěrů ze stavebního spoření? To zjistíte u našich kvalifikovaných
poradců."
                ]
            ),
            new SectionDataObj(
                "PODNIKATELSKÉ<br>ÚVĚRY",
                "",
                "company-loans",
                "podnikatelske_uvery.svg",
                "podnikatelske_uvery-vyber.svg",
                [
                    "Podnikatelské úvěry jsou určeny pro právnické osoby nebo fyzické osoby podnikající.
Mohou být účelové i neúčelové, formou klasické půjčky či kontokorentu. Využívají se
především k financování např. pořízení nemovitosti, strojů a výpočetní techniky. Náklady na
úvěr si podnikatelské subjekty mohou dát do nákladů."
                ]
            )

        );
    }
}
