<?php

namespace App\dataObj\staticData;


use App\dataObj\declarations\PositionsDataObj;
use App\dataObj\declarations\PositionSectionDataObj;

class Positions
{
    public static function getData()
    {
        return array(
            new PositionsDataObj(
                "benefity",
                "Naše benefity",
                [
                    new PositionSectionDataObj(
                        "BENEFITY PRO SPOLUPRACOVNÍKY NABÍZENÉ NAŠÍ SPOLEČNOSTÍ",
                        "fas fa-plus",
                        [
                            'Získání potřebných licencí a registrací',

                            'Zcela bezplatné vzdělávání a školení',
                            'Motivační finanční odměňování, bez limitu',
                            '„Operák“ na auto',
                            'Bezplatné využití kancelářských prostorů, techniky pro každého spolupracovníka',
                            'Slevy v rámci naší společnosti a našich partnerů',
                            'Podíl na zisku společnosti'
                        ]
                    )
                ]
            ),
            new PositionsDataObj(
                "poradce",
                "Poradce",

                [
                    new PositionSectionDataObj(
                        "Co Vás čeká?",
                        "fas fa-plus",
                        [
                            'poskytovat poradenství klientům v oblasti produktového portfolia',
                            'zpracovávat požadavky klientů o hypoteční a spotřebitelské úvěry, pojištění, investice (zaměření je možné i na jednu činnost)',
                            'realizovat obchodní případy až do úrovně uzavření a následné péče',
                            'práce v našem interním systému i v systémech našich partnerů (zaškolení je samozřejmostí)'
                        ]
                    ),
                    new PositionSectionDataObj(
                        "Co od Vás požadujeme?",
                        "fas fa-plus",
                        [
                            'min. SŠ/VŠ vzdělání',
                            'výhodou jsou akreditační zkoušky',
                            'přehled o situaci na trhu, praktické zkušenosti',
                            'ŘP skupiny B',
                            'vlastní názor a schopnost naslouchat'
                        ]
                    )
                ]
            ),
            new PositionsDataObj(
                "manager",
                "Manažer",

                [
                    new PositionSectionDataObj(
                        "Co Vás čeká?",
                        "fas fa-plus",
                        [
                            'Řízení obchodního týmu',
                            'Odpovědnost za svěřený region',
                            'Velké kompetence pro personální práci',
                            'realizovat obchodní případy až do úrovně uzavření a následné péče',
                            'práce v našem interním systému i v systémech našich partnerů'
                        ]
                    ),
                    new PositionSectionDataObj(
                        "Co od Vás požadujeme?",
                        "fas fa-plus",
                        [
                            'min. SŠ/VŠ vzdělání',
                            'Schopnost řídit obchodní tým',
                            'přehled o situaci na trhu, praktické zkušenosti',
                            'vlastní názor a schopnost naslouchat'
                        ]
                    )
                ]
            )
        );
    }
}
