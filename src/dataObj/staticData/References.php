<?php

namespace App\dataObj\staticData;


use App\dataObj\declarations\ReferenceDataObj;

class References
{
    public static function getData()
    {
        return array(
            new ReferenceDataObj(
              "chovatele_logo.png",
              "http://www.cschdz.eu/",
                ['w-30', 'h-30']
            ), new ReferenceDataObj(
              "Crassusart_reference_loga_cyklord.svg",
              "http://cyklord.cz"
            ),
            new ReferenceDataObj(
                "Crassusart_reference_loga_cubeteam.svg",
                "http://www.cubeteam.eu"
            ),
            new ReferenceDataObj(
                "Crassusart_reference_loga_acp.svg",
                "https://www.acpe.cz"
            ),
            new ReferenceDataObj(
                "Crassusart_reference_loga_bkg.svg",
                "http://www.bkg.cz"
            ),
            new ReferenceDataObj(
                "Crassusart_reference_loga_vlajkovestozary.svg",
                "http://vlajkovestozary.cz"
            ),
            new ReferenceDataObj(
                "Crassusart_reference_loga_je-art.svg",
                "https://www.je-art.cz"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_obec.svg",
                "https://www.horni-ujezd.cz"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_diakonie.svg",
                "http://www.diakoniebroumov.cz"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_sorke.svg",
                "https://www.sorke.cz"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_saint_medical.svg",
                "https://saintmedical.cz"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_arkov.svg",
                "https://www.arkov.cz"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_chroustovice.svg",
                "http://www.obec-chroustovice.net"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_kovomont.svg",
                "http://www.kovomont-pce.cz"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_ladesign.svg",
                "http://www.ladesign.cz"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_moldcom.svg",
                "http://www.moldcom.cz"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_sokol.svg",
                "#"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_SVJ_dasicka.svg",
                "#"
            ),new ReferenceDataObj(
                "Crassusart_reference_loga_SVJ_salavcova.svg",
                "#"
            ),
            new ReferenceDataObj(
                "Crassusart_reference_moravany.svg",
                "#"
            ),
            new ReferenceDataObj(
                "Crassusart_reference_valy.svg",
                "#"
            ),
        );
    }
}
